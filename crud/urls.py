from django.urls import path
from . import views

urlpatterns = [
    path('', views.lee_usuario, name='lee_usuario'),
    path('crea/', views.crea_usuario, name='crea_usuario'),
    path('actualiza/<id>', views.actualiza_usuario, name='actualiza_usuario'),
    path('elimina/<id>', views.elimina_usuario, name='elimina_usuario'),

    path('api/', views.lee_usuario_api, name='lee_usuario_api'),
    path('crea_api/', views.crea_usuario_api, name='crea_usuario_api'),
    path('actualiza_api/<id>', views.actualiza_usuario_api, name='actualiza_usuario_api'),
    path('elimina_api/<id>', views.elimina_usuario_api, name='elimina_usuario_api'),
    path('consulta_api/<id>', views.consulta_usuario_api, name='consulta_usuario_api'),
]