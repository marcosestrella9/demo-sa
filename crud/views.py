import json

from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt

from crud import models
from crud.models import Usuario
from crud.forms import UsuarioForm
# Create your views here.

# Funcion para obtener los usuarios y buscador por nombre
@login_required(login_url='/login/')
def lee_usuario(request):
    if request.method == 'POST':
        nombre = request.POST.getlist('nombre')
        usuarios = Usuario.objects.all().values('id', 'nombre', 'empresa', 'correo', 'rol')
        if nombre[0]:
            usuarios = usuarios.filter(nombre__contains=nombre[0])
        context = {
            'usuarios': list(usuarios)
        }
        return JsonResponse(context)
    usuarios = Usuario.objects.all()
    context = {
        'usuarios': usuarios
    }
    return render(request, 'usuarios.html', context)

# Funcion para crear usuarios
@login_required(login_url='/login/')
def crea_usuario(request):
    if request.method == 'POST':
        form = UsuarioForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/crud')
    else:
        form = UsuarioForm()

    return render(request, 'admin_usuarios.html', {'form': form})

# Funcion para actualizar usuarios
@login_required(login_url='/login/')
def actualiza_usuario(request, id):
    usuario = Usuario.objects.get(id=id)
    form = UsuarioForm(request.POST or None, instance=usuario)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect("/crud")
    context = {
        'form': form
    }
    return render(request, 'admin_usuarios.html', context)

# Funcion para eliminar usuarios
@login_required(login_url='/login/')
def elimina_usuario(request, id):
    usuario = Usuario.objects.get(id=id)
    usuario.delete()
    return HttpResponseRedirect("/crud")


# FUNCIONES API

# Funcion para obtener los usuarios api y buscador por nombre
@csrf_exempt
def lee_usuario_api(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        nombre = data['nombre']
        usuarios = Usuario.objects.all().values('id', 'nombre', 'empresa', 'correo', 'rol')
        if nombre:
            usuarios = usuarios.filter(nombre__contains=nombre)
        context = {
            'usuarios': list(usuarios)
        }
        return JsonResponse(context)
    usuarios = Usuario.objects.all().values('id', 'nombre', 'empresa', 'correo', 'rol')
    context = {
        'usuarios': list(usuarios)
    }
    return JsonResponse(context)

# Funcion para crear usuarios api
@csrf_exempt
def crea_usuario_api(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        usuario = models.Usuario()
        usuario.nombre=data['nombre']
        usuario.empresa = data['empresa']
        usuario.correo = data['correo']
        usuario.rol = data['rol']
        usuario.save()
        return HttpResponse('<h1>Usuario creado</h1>')

# Funcion para obtener usuario especifico api
@csrf_exempt
def consulta_usuario_api(request,id):
    usuario = Usuario.objects.filter(id=id).values('id', 'nombre', 'empresa', 'correo', 'rol')
    context = {
        'usuario': list(usuario)[0]
    }
    return JsonResponse(context, safe=False)


# Funcion para actualizar usuarios api
@csrf_exempt
def actualiza_usuario_api(request, id):
    if request.method == 'PUT':
        data = json.loads(request.body)
        usuario = Usuario.objects.get(id=id)
        usuario.nombre=data['nombre']
        usuario.empresa = data['empresa']
        usuario.correo = data['correo']
        usuario.rol = data['rol']
        usuario.save()
        return HttpResponse('<h1>Usuario actualizado</h1>')

# Funcion para eliminar usuarios
@csrf_exempt
def elimina_usuario_api(request, id):
    if request.method == 'DELETE':
        usuario = Usuario.objects.get(id=id)
        usuario.delete()
        return HttpResponse('<h1>Usuario eliminado</h1>')