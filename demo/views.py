from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect


def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect("/crud")
        return render(request, "login.html", {"message": "Credenciales incorrectas"})
    else:
        return render(request, 'login.html')


@login_required(login_url='/login/')
def logout_view(request):
    logout(request)
    return render(request, "login.html", {"message": "Sesion cerrada."})
